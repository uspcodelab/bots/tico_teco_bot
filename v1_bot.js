const TelegramBot = require('node-telegram-bot-api');

const token = '700621141:AAGY_rJokx7yDUl-VMTqIk4x8lIHeIWoURI';

const bot = new TelegramBot(token, {polling: true});

const allTasks = [];

function listToMessage(list) {
  let i;
  let msg = "Tarefas:\n";
  for (i = 0; i < list.length; i++) {
    msg += `- ${list[i]}\n`;
  }

  return msg;
}

bot.on('message', (msg) => {
  const text = msg.text,
        parts = text.split(" "),
        chat = msg.chat,
        chat_id = chat.id;


  if (parts[0] == "/add") {
    let i;
    let task = "";
    for (i = 1; i < parts.length; i++) {
      task = task + " " + parts[i];
    }

    allTasks.push(task);
    bot.sendMessage(chat_id, `Added task: "${task}"`);
  } else if (parts[0] == "/getAll") {
    bot.sendMessage(chat_id, listToMessage(allTasks));
  } else {
    bot.sendMessage(chat_id, "[nada a fazer]");
  }
});
