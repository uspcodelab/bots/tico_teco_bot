FROM node:alpine

WORKDIR /usr/src/bot

RUN npm i -g nodemon \
    && npm i -s node-telegram-bot-api

COPY *.js ./

# nodemon monitors for changes in file
# and auto reload if something happens
CMD nodemon bot.js
