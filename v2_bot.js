const TelegramBot = require('node-telegram-bot-api');
const fs = require('fs');

const token = '700621141:AAGY_rJokx7yDUl-VMTqIk4x8lIHeIWoURI';

const bot = new TelegramBot(token, {polling: true});

let allTasks = [];

function listToMessage(list) {
  let i;
  let msg = "Tarefas:\n";
  for (i = 0; i < list.length; i++) {
    msg += `- ${list[i]}\n`;
  }

  return msg;
}

bot.onText(/\/add (.+)/, (msg, match) => {
  let task = match[1];
  const chat = msg.chat,
        chat_id = chat.id;

  allTasks.push(task);
  bot.sendMessage(chat_id, `Added task: "${task}"`);
});

bot.onText(/\/getAll/, (msg, match) => {
  const chat = msg.chat,
        chat_id = chat.id;

  bot.sendMessage(chat_id, listToMessage(allTasks));
});
