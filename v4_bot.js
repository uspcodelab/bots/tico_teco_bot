const TelegramBot = require('node-telegram-bot-api');
const fs = require('fs');

const token = '700621141:AAGY_rJokx7yDUl-VMTqIk4x8lIHeIWoURI';

const bot = new TelegramBot(token, {polling: true});

let allTasks = [];
let scores = [];

function listToMessage(list) {
  let i;
  let msg = "Tarefas:\n";
  for (i = 0; i < list.length; i++) {
    msg += `- ${list[i].task}\n`;
  }

  return msg;
}

bot.onText(/\/add (.+)/, (msg, match) => {
  let task = match[1];
  const chat = msg.chat,
        chat_id = chat.id;

  allTasks.push(
    { task: task, owner: msg.from.username }
  );

  scores.push(
    { owner: msg.from.username, score: 0 }
  );

  bot.sendMessage(chat_id, `Added task: "${task}"`);
});

bot.onText(/\/getAll/, (msg, match) => {
  const chat = msg.chat,
        chat_id = chat.id;

  let userTasks = allTasks.filter(t => t.owner == msg.from.username);
  bot.sendMessage(chat_id, listToMessage(userTasks));
});

bot.onText(/\/delete (.+)/, (msg, match) => {
  let task = match[1];
  const chat = msg.chat,
        chat_id = chat.id;

  let userTasks = allTasks.filter(t => t.owner == msg.from.username);

  userTasks = userTasks.filter(item => item.task != task);
  bot.sendMessage(chat_id, `Deleted task: "${task}"`);
  bot.sendMessage(chat_id, listToMessage(userTasks));
});

bot.onText(/\/done (.+)/, (msg, match) => {
  let task = match[1];
  const chat = msg.chat,
        chat_id = chat.id;

  let userTasks = allTasks.filter(item => item.owner == msg.from.username);
  
  // problema: usa ponteiros
  let s = scores.find(item => item.owner == msg.from.username);
  s.score++;

  // problema: explicita índice de lista
  // let i = scores.findIndex(item => item.owner == msg.from.username);
  // scores[i].score++;


  userTasks = userTasks.filter(item => item.task != task);
  bot.sendMessage(chat_id, `Congratulations! You just marked "${task}" as done and scored +1`);
  bot.sendMessage(chat_id, listToMessage(userTasks));
});

bot.onText(/\/status/, (msg, match) => {
  const chat = msg.chat,
        chat_id = chat.id;

  // repete problemas anteriores
  let s = scores.find(item => item.owner == msg.from.username);
  let score = s.score;

  bot.sendMessage(chat_id, `Your current score is: ${score}`);
});
