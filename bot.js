const TelegramBot = require('node-telegram-bot-api');

const token = '799839448:AAGSJ95KiWVveC0LM45KelicuPV7sJJr4Ek';

const bot = new TelegramBot(token, {polling: true});
const allTasks = [];



/* isto é um comentário

/a/

+ a
- aa, asa, b, ~

-------

/a+/

+ a, aa, aaa, aaaa, ...
- ab, abc, aaab


--------

/a\+/

+ a+
- aa+, a++

--------

/a\++/

+ a++, a+++++, a+


---------


/a\+{1,5}

+ a+, a++, a+++, a++++, a+++++
- a++++++


---------

/(a\+)+/
/(a\+){2,4}/


-------

/\_\_(.+)/
/\_\_(.*)/


+ __abc, __a, __oierjsdjj

/R\$\ *[0-9]{1,8}(,[0-9]{2})?/

R$4
R$4,00

/[aeiou]{5}/

/[a-zA-Z]/



---------------------

/start
/add
/getAll



/\/add\ (.+)/

+ /add minha lição de casa, 



-------------

abacate
amarelo
azul
arte
artifício
artesão
a


*/




bot.onText(/\/start/, (msg) => {
  bot.sendMessage(msg.chat.id, "Olá, bem vindo!");
});


bot.onText(/\/add\ +(.+)/, (msg, match) => {
  allTasks.push(
    { userTask: match[1], user: msg.chat.id }
  );

  bot.sendMessage(msg.chat.id, `Tarefa adicionada "${match[1]}"`);
});

bot.onText(/\/getAll/, (msg) => {
  let j;
  let tarefas = "Tarefas:\n";

  const userTasks = allTasks.filter(t => t.user == msg.chat.id);

  for (j = 0; j < userTasks.length; j++) {
    tarefas += ` - ${userTasks[j].userTask}\n`;
  }

  bot.sendMessage(msg.chat.id, tarefas);

});

/*

bot.on('message', (msg) => {

  const text = msg.text;
  const words = text.split(" ");



  if (text == '/start') {
    
    bot.sendMessage(msg.chat.id, "Olá, bem vindo!");

  }

  else if (words[0] == "/add") {
    let i;
    let task = "";

    for (i = 1; i < words.length; i++) {
      task = task + " " + words[i];
    }

    allTasks.push(
      { userTask: task, user: msg.chat.id }
    );

    bot.sendMessage(msg.chat.id, `Tarefa Adicionada "${task}"`);
  }

  else if (words[0] == "/getAll") {
    let j;
    let tarefas = "Tarefas:\n";
    
    const userTasks = allTasks.filter(t => t.user == msg.chat.id);

    for (j = 0; j < userTasks.length; j++) {
      tarefas += ` - ${userTasks[j].userTask}\n`;
    }

    bot.sendMessage(msg.chat.id, tarefas);
  }

});

*/
